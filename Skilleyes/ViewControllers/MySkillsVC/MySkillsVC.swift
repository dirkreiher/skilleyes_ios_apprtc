//
//  MySkillsVC.swift
//  Skilleyes
//
//  Created by Mac HD on 28/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class MySkillsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //Outlets
    @IBOutlet var tblVewList: UITableView!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var lblAdSkil: UILabel!
    
    //Variable
    let STATUSBAR_COLOR = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        changeLanguage()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //MARK: app language method
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            lblAdSkil.text = "Fertigkeit hinzufügen"
            lblHeading.text = "meine Fähigkeiten"
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            lblAdSkil.text = "Add Skill"
            lblHeading.text = "mySkills"
        }
    }
    
    //MARK: action method
    @IBAction func actionAdd(_ sender: Any)
    {
        let objLoginVC:MySkillEditVC = storyboard?.instantiateViewController(withIdentifier: "MySkillEditVC") as! MySkillEditVC
        self.navigationController?.pushViewController(objLoginVC, animated: false)
    }
    
    @IBAction func actionBack(_ sender: Any)
    {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    //MARK: tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellSkill") as? CellSkill
        if cell == nil {
            let tempArray = Bundle.main.loadNibNamed("CellSkill", owner: self, options: nil)
            cell = tempArray?.first  as? CellSkill
        }
        
        cell?.btnEdit.addTarget(self, action: #selector(actionEdit(sender:)), for: .touchUpInside)
        cell?.btnEdit.tag = indexPath.row
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 108
    }

    //MARK: tableview action method
    @objc func actionEdit(sender:UIButton)
    {
        let objLoginVC:MySkillEditVC = storyboard?.instantiateViewController(withIdentifier: "MySkillEditVC") as! MySkillEditVC
        self.navigationController?.pushViewController(objLoginVC, animated: false)
    }
    
}
