//
//  Validation.swift
//  curency
//
//  Created by WebvilleeMac3 on 02/10/17.
//  Copyright © 2017 WebvilleeMac3. All rights reserved.
//

import UIKit

class Validation: NSObject {
    
    //MARK: Validation Methods.
    func isNameValid(_ nameStr :String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let output: String = nameStr.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (nameStr == output)
        print("\(isValid)")
        return isValid
    }
    
    func isEmailValid(_ emailStr: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,30}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: emailStr)
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])[A-Za-z\\d$@$#!%*?&]{6,30}")
        return passwordTest.evaluate(with: password)
    }
    /*
        1 - Password length is 8.
        2 - 2 UpperCase letters in Password.
        3 - One Special Character in Password.
        4 - Two Number in Password.
        5- Three letters of lowercase in password.
    */
   /* func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$")
        return passwordTest.evaluate(with: password)
    }*/
    
    func isNumberValid(_ value: String) -> Bool {
        let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    func isValid(_ value: String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
        let output: String = value.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (value == output)
        print("\(isValid)")
        return isValid
    }
   
    func isValidZipCode(_ pincode: String) -> Bool {
        let pinRegex = "^[0-9]{10}$"
        let pinTest = NSPredicate(format: "SELF MATCHES %@", pinRegex)
        let pinValidates: Bool = pinTest.evaluate(with: pincode)
        return pinValidates
    }
}

