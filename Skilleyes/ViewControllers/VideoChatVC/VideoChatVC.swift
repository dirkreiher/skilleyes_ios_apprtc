//
//  VideoChatVC.swift
//  Skilleyes
//
//  Created by Dirk Reiher on 08.01.19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit

class VideoChatVC: UIViewController {
    
    //Variables
    let STATUSBAR_COLOR = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    var mode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
            
    }
    
    //Set Statusbar Color
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
}
