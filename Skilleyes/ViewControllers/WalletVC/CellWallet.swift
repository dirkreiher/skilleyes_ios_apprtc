//
//  CellWallet.swift
//  Skilleyes
//
//  Created by Mac HD on 28/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class CellWallet: UITableViewCell {

    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblmySkil: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var btnInfo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
