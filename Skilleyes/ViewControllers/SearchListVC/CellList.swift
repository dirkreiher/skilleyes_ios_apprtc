//
//  CellList.swift
//  Skilleyes
//
//  Created by Mac HD on 23/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class CellList: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
