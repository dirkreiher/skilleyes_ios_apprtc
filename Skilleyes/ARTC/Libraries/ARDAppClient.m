/*
 * libjingle
 * Copyright 2014, Google Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *  3. The name of the author may not be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "ARDAppClient.h"
#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>

#import "ARDMessageResponse.h"
#import "ARDRegisterResponse.h"
#import "ARDSignalingMessage.h"
#import "ARDUtilities.h"
#import "ARDWebSocketChannel.h"
#import "RTCICECandidate+JSON.h"
#import "RTCICEServer+JSON.h"
#import "RTCMediaConstraints.h"
#import "RTCMediaStream.h"
#import "RTCPair.h"
#import "RTCPeerConnection.h"
#import "RTCPeerConnectionDelegate.h"
#import "RTCPeerConnectionFactory.h"
#import "RTCSessionDescription+JSON.h"
#import "RTCSessionDescriptionDelegate.h"
#import "RTCVideoCapturer.h"
#import "RTCVideoTrack.h"

// TODO(tkchin): move these to a configuration object.
static NSString *kARDRoomServerHostUrl = @"https://appr.tc/";
static NSString *kARDRoomServerRegisterFormat = @"%@/join/%@";
static NSString *kARDRoomServerMessageFormat = @"%@/message/%@/%@";
static NSString *kARDRoomServerByeFormat = @"%@/leave/%@/%@";
static NSString *kARDDefaultSTUNServerUrl = @"stun:stun.l.google.com:19302";
//static NSString *kARDTurnRequestUrl = @"https://computeengineondemand.appspot.com"

static NSString *kARDAppClientErrorDomain = @"ARDAppClient";
static NSInteger kARDAppClientErrorUnknown = -1;
static NSInteger kARDAppClientErrorRoomFull = -2;
static NSInteger kARDAppClientErrorCreateSDP = -3;
static NSInteger kARDAppClientErrorSetSDP = -4;
static NSInteger kARDAppClientErrorNetwork = -5;
static NSInteger kARDAppClientErrorInvalidClient = -6;
static NSInteger kARDAppClientErrorInvalidRoom = -7;

@interface ARDAppClient () <ARDWebSocketChannelDelegate, RTCPeerConnectionDelegate, RTCSessionDescriptionDelegate>
    
    @property(nonatomic, strong) ARDWebSocketChannel *channel;
    @property(nonatomic, strong) RTCPeerConnection *peerConnection;
    @property(nonatomic, strong) RTCPeerConnectionFactory *factory;
    @property(nonatomic, strong) NSMutableArray *messageQueue;
    
    @property(nonatomic, assign) BOOL isTurnComplete;
    @property(nonatomic, assign) BOOL hasReceivedSdp;
    @property(nonatomic, readonly) BOOL isRegisteredWithRoomServer;
    
    @property(nonatomic, strong) NSString *roomId;
    @property(nonatomic, strong) NSString *clientId;
    @property(nonatomic, assign) BOOL isInitiator;
    @property(nonatomic, assign) BOOL isSpeakerEnabled;
    @property(nonatomic, strong) NSMutableArray *iceServers;
    @property(nonatomic, strong) NSURL *webSocketURL;
    @property(nonatomic, strong) NSURL *webSocketRestURL;
    @property(nonatomic, strong) RTCAudioTrack *defaultAudioTrack;
    @property(nonatomic, strong) RTCVideoTrack *defaultVideoTrack;
    
@end

@implementation ARDAppClient
    
    @synthesize delegate = _delegate;
    @synthesize state = _state;
    @synthesize serverHostUrl = _serverHostUrl;
    @synthesize channel = _channel;
    @synthesize peerConnection = _peerConnection;
    @synthesize factory = _factory;
    @synthesize messageQueue = _messageQueue;
    @synthesize isTurnComplete = _isTurnComplete;
    @synthesize hasReceivedSdp  = _hasReceivedSdp;
    @synthesize roomId = _roomId;
    @synthesize clientId = _clientId;
    @synthesize isInitiator = _isInitiator;
    @synthesize isSpeakerEnabled = _isSpeakerEnabled;
    @synthesize iceServers = _iceServers;
    @synthesize webSocketURL = _websocketURL;
    @synthesize webSocketRestURL = _websocketRestURL;
    
    //Init Delegate
    - (instancetype)initWithDelegate:(id<ARDAppClientDelegate>)delegate {
        if (self = [super init]) {
            _delegate = delegate;
            _factory = [[RTCPeerConnectionFactory alloc] init];
            _messageQueue = [NSMutableArray array];
            _iceServers = [NSMutableArray arrayWithObject:[self defaultSTUNServer]];
            _serverHostUrl = kARDRoomServerHostUrl;
            _isSpeakerEnabled = YES;
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(orientationChanged:)
                                                         name:@"UIDeviceOrientationDidChangeNotification"
                                                       object:nil];
            }
        return self;
    }
    
    //Dealloc Disconnect
    - (void)dealloc {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIDeviceOrientationDidChangeNotification" object:nil];
        [self disconnect];
    }
    
    //Orientation Change
    - (void)orientationChanged:(NSNotification *)notification {
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        if (UIDeviceOrientationIsLandscape(orientation) || UIDeviceOrientationIsLandscape(orientation)) {
            //Remove current video track
            RTCMediaStream *localStream = _peerConnection.localStreams[0];
            [localStream removeVideoTrack:localStream.videoTracks[0]];
            
            RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];
            if (localVideoTrack) {
                [localStream addVideoTrack:localVideoTrack];
                [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
            }
            
            [_peerConnection removeStream:localStream];
            [_peerConnection addStream:localStream];
        }
    }
    
    //Set ARDApp State
    - (void)setState:(ARDAppClientState)state {
        if (_state == state) {
            return;
        }
        _state = state;
        [_delegate appClient:self didChangeState:_state];
    }
    
    - (void)connectToRoomWithId:(NSString *)roomId options:(NSDictionary *) options {
        
        NSParameterAssert(roomId.length);
        NSParameterAssert(_state == kARDAppClientStateDisconnected);
        self.state = kARDAppClientStateConnecting;
        
        // Request TURN.
        __weak ARDAppClient *weakSelf = self;
        
        //Set Xirsys Turn Server Manuell
        NSString *username = @"603febb8-5e6e-11e8-8742-3a11f5d3b782";
        NSString *password = @"603fec26-5e6e-11e8-ab1a-a6a06791d4fd";
        
        NSMutableArray *turnServer = [NSMutableArray arrayWithCapacity: 7];
        RTCICEServer *server1 = [[RTCICEServer alloc] initWithURI:[NSURL URLWithString: @"turn:e5.xirsys.com:80?transport=tcp"] username: username password: password];
        RTCICEServer *server2 = [[RTCICEServer alloc] initWithURI:[NSURL URLWithString: @"turn:e5.xirsys.com:80?transport=udp"] username: username password: password];
        RTCICEServer *server3 = [[RTCICEServer alloc] initWithURI:[NSURL URLWithString: @"turn:e5.xirsys.com:3478?transport=udp"] username: username password: password];
        RTCICEServer *server4 = [[RTCICEServer alloc] initWithURI:[NSURL URLWithString: @"turn:e5.xirsys.com:80?transport=tcp"] username: username password: password];
        RTCICEServer *server5 = [[RTCICEServer alloc] initWithURI:[NSURL URLWithString: @"turn:e5.xirsys.com:3478?transport=tcp"] username: username password: password];
        RTCICEServer *server6 = [[RTCICEServer alloc] initWithURI:[NSURL URLWithString: @"turns:e5.xirsys.com:443?transport=tcp"] username: username password: password];
        RTCICEServer *server7 = [[RTCICEServer alloc] initWithURI:[NSURL URLWithString: @"turns:e5.xirsys.com:5349?transport=tcp"] username: username password: password];
        
        [turnServer addObject:server1];
        [turnServer addObject:server2];
        [turnServer addObject:server3];
        [turnServer addObject:server4];
        [turnServer addObject:server5];
        [turnServer addObject:server6];
        [turnServer addObject:server7];
        
        ARDAppClient *strongSelf = weakSelf;
        [strongSelf.iceServers addObjectsFromArray: turnServer];
        strongSelf.isTurnComplete = YES;
        [strongSelf startSignalingIfReady];
        
        // Register with room server.
        [self registerWithRoomServerForRoomId:roomId completionHandler:^(ARDRegisterResponse *response) {
            ARDAppClient *strongSelf = weakSelf;
            
            if (!response || response.result == kARDRegisterResultTypeFull) {
                //Failed to register With Room
                NSLog(@"Failed to register with room server. Result:%d", (int)response.result);
                [strongSelf disconnect];
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: @"Room is full."
                                           };
                NSError *error =
                [[NSError alloc] initWithDomain:kARDAppClientErrorDomain
                                           code:kARDAppClientErrorRoomFull
                                       userInfo:userInfo];
                [strongSelf.delegate appClient:strongSelf didError:error];
                return;
            } else if (response.result ==  kARDRegisterResultTypeUnknown) {
                //Error in parsing response data
                [strongSelf disconnect];
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: @"Unknown error occurred."
                                           };
                NSError *error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain code:kARDAppClientErrorUnknown userInfo:userInfo];
                [strongSelf.delegate appClient:strongSelf didError:error];
                return;
            }
            //Success
            NSLog(@"Registered with room server.");
            strongSelf.roomId = response.roomId;
            strongSelf.clientId = response.clientId;
            strongSelf.isInitiator = response.isInitiator;
            
            for (ARDSignalingMessage *message in response.messages) {
                if (message.type == kARDSignalingMessageTypeOffer || message.type == kARDSignalingMessageTypeAnswer) {
                    strongSelf.hasReceivedSdp = YES;
                    [strongSelf.messageQueue insertObject:message atIndex:0];
                } else {
                    [strongSelf.messageQueue addObject:message];
                }
            }
            
            //Set WebSocket
            NSLog(@"WEBSOCKET");
            NSLog(@"%@", response.webSocketURL);
            NSLog(@"%@", response.webSocketRestURL);
            
            strongSelf.webSocketURL = response.webSocketURL;
            strongSelf.webSocketRestURL = response.webSocketRestURL;
            [strongSelf registerWithColliderIfReady];
            [strongSelf startSignalingIfReady];
        }];
    }
    
    //Disconnect
    - (void)disconnect {
        if (_state == kARDAppClientStateDisconnected) {
            return;
        }
        if (self.isRegisteredWithRoomServer) {
            [self unregisterWithRoomServer];
        }
        if (_channel) {
            if (_channel.state == kARDWebSocketChannelStateRegistered) {
                // Tell the other client we're hanging up.
                ARDByeMessage *byeMessage = [[ARDByeMessage alloc] init];
                NSData *byeData = [byeMessage JSONData];
                [_channel sendData:byeData];
            }
            // Disconnect from collider.
            _channel = nil;
        }
        _clientId = nil;
        _roomId = nil;
        _isInitiator = NO;
        _hasReceivedSdp = NO;
        _messageQueue = [NSMutableArray array];
        _peerConnection = nil;
        self.state = kARDAppClientStateDisconnected;
    }
    
    #pragma mark - ARDWebSocketChannelDelegate
    //Channel Did Receive Message
    - (void)channel:(ARDWebSocketChannel *)channel didReceiveMessage:(ARDSignalingMessage *)message {
        switch (message.type) {
            case kARDSignalingMessageTypeOffer:
                //Empty
            case kARDSignalingMessageTypeAnswer:
                _hasReceivedSdp = YES;
                [_messageQueue insertObject:message atIndex:0];
                break;
            
            case kARDSignalingMessageTypeCandidate:
                [_messageQueue addObject:message];
                break;
            
            case kARDSignalingMessageTypeBye:
                [self processSignalingMessage:message];
                return;
        }
        [self drainMessageQueueIfReady];
    }
    
    //Channel DidChange State
    - (void)channel:(ARDWebSocketChannel *)channel didChangeState:(ARDWebSocketChannelState)state {
        switch (state) {
            case kARDWebSocketChannelStateOpen:
                //Empty
                break;
            
            case kARDWebSocketChannelStateRegistered:
                //Empty
                break;
            
            case kARDWebSocketChannelStateClosed:
                //Empty
            
            case kARDWebSocketChannelStateError:
                [self disconnect];
                break;
        }
    }
    
    #pragma mark - RTCPeerConnectionDelegate
    //Signal State PeerConnection
    - (void)peerConnection:(RTCPeerConnection *)peerConnection signalingStateChanged:(RTCSignalingState)stateChanged {
        NSLog(@"Signaling state changed: %d", stateChanged);
    }
    
    //Add Stream PeerConnection
    - (void)peerConnection:(RTCPeerConnection *)peerConnection addedStream:(RTCMediaStream *)stream {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Received %lu video tracks and %lu audio tracks",
                  (unsigned long)stream.videoTracks.count,
                  (unsigned long)stream.audioTracks.count);
            if (stream.videoTracks.count) {
                RTCVideoTrack *videoTrack = stream.videoTracks[0];
                [self->_delegate appClient:self didReceiveRemoteVideoTrack:videoTrack];
                if (self->_isSpeakerEnabled) [self enableSpeaker]; //Use the "handsfree" speaker instead of the ear speaker.
            }
        });
    }
    
    //Remove Stream Peerconnection LOG
    - (void)peerConnection:(RTCPeerConnection *)peerConnection removedStream:(RTCMediaStream *)stream {
        NSLog(@"Stream was removed.");
    }
    
    //Renegotiation PeerConnnection LOG
    - (void)peerConnectionOnRenegotiationNeeded: (RTCPeerConnection *)peerConnection {
        NSLog(@"WARNING: Renegotiation needed but unimplemented.");
    }
    
    //Ice Connection Change LOG
    - (void)peerConnection:(RTCPeerConnection *)peerConnection iceConnectionChanged:(RTCICEConnectionState)newState {
        NSLog(@"ICE state changed: %d", newState);
    }
    
    //Ice Gathering Change LOG
    - (void)peerConnection:(RTCPeerConnection *)peerConnection iceGatheringChanged:(RTCICEGatheringState)newState {
        NSLog(@"ICE gathering state changed: %d", newState);
    }
    
    //Got Ice Candidate
    - (void)peerConnection:(RTCPeerConnection *)peerConnection gotICECandidate:(RTCICECandidate *)candidate {
        dispatch_async(dispatch_get_main_queue(), ^{
            ARDICECandidateMessage *message =[[ARDICECandidateMessage alloc] initWithCandidate:candidate];
            [self sendSignalingMessage:message];
        });
    }
    
    //Did Open Data Channel
    - (void)peerConnection:(RTCPeerConnection*)peerConnection didOpenDataChannel:(RTCDataChannel*)dataChannel {
        //Empty
    }
    
    #pragma mark - RTCSessionDescriptionDelegate
    //Did Create Session Descripton
    - (void)peerConnection:(RTCPeerConnection *)peerConnection didCreateSessionDescription:(RTCSessionDescription *)sdp error:(NSError *)error {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                NSLog(@"Failed to create session description. Error: %@", error);
                [self disconnect];
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: @"Failed to create session description.",
                                           };
                NSError *sdpError = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain code:kARDAppClientErrorCreateSDP userInfo:userInfo];
                [self->_delegate appClient:self didError:sdpError];
                return;
            }
            [self->_peerConnection setLocalDescriptionWithDelegate:self sessionDescription:sdp];
            ARDSessionDescriptionMessage *message = [[ARDSessionDescriptionMessage alloc] initWithDescription:sdp];
            [self sendSignalingMessage:message];
        });
    }
    
    //Session Description with Error
    - (void)peerConnection:(RTCPeerConnection *)peerConnection didSetSessionDescriptionWithError:(NSError *) error {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                NSLog(@"Failed to set session description. Error: %@", error);
                [self disconnect];
                NSDictionary *userInfo = @{
                                           NSLocalizedDescriptionKey: @"Failed to set session description.",
                                           };
                NSError *sdpError = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain code:kARDAppClientErrorSetSDP userInfo:userInfo];
                [self->_delegate appClient:self didError:sdpError];
                return;
            }
            // If we're answering and we've just set the remote offer we need to create
            // an answer and set the local description.
            if (!self->_isInitiator && !self->_peerConnection.localDescription) {
                RTCMediaConstraints *constraints = [self defaultAnswerConstraints];
                [self->_peerConnection createAnswerWithDelegate:self constraints:constraints];
            }
        });
    }
    
    #pragma mark - Private
    //Is Registred with Room Server
    - (BOOL)isRegisteredWithRoomServer {
        return _clientId.length;
    }
    
    //Start Signaling if Ready
    - (void)startSignalingIfReady {
        if (!_isTurnComplete || !self.isRegisteredWithRoomServer) {
            return;
        }
        self.state = kARDAppClientStateConnected;
        
        // Create peer connection.
        RTCMediaConstraints *constraints = [self defaultPeerConnectionConstraints];
        _peerConnection = [_factory peerConnectionWithICEServers:_iceServers constraints:constraints delegate:self];
        RTCMediaStream *localStream = [self createLocalMediaStream];
        [_peerConnection addStream:localStream];
        if (_isInitiator) {
            [self sendOffer];
        } else {
            [self waitForAnswer];
        }
    }
    
    //Send Offer
    - (void)sendOffer {
        [_peerConnection createOfferWithDelegate:self constraints:[self defaultOfferConstraints]];
    }
    
    //Wait for Answer
    - (void)waitForAnswer {
        [self drainMessageQueueIfReady];
    }
    
    //Drain Message Queue
    - (void)drainMessageQueueIfReady {
        if (!_peerConnection || !_hasReceivedSdp) {
            return;
        }
        
        for (ARDSignalingMessage *message in _messageQueue) {
            [self processSignalingMessage:message];
        }
        
        [_messageQueue removeAllObjects];
    }
    
    //Process Signaling Message
    - (void)processSignalingMessage:(ARDSignalingMessage *) message {
        NSParameterAssert(_peerConnection || message.type == kARDSignalingMessageTypeBye);
        switch (message.type) {
            case kARDSignalingMessageTypeOffer:
            //Empty
            case kARDSignalingMessageTypeAnswer: {
                ARDSessionDescriptionMessage *sdpMessage = (ARDSessionDescriptionMessage *) message;
                RTCSessionDescription *description = sdpMessage.sessionDescription;
                [_peerConnection setRemoteDescriptionWithDelegate:self sessionDescription:description];
                break;
            }
            
            case kARDSignalingMessageTypeCandidate: {
                ARDICECandidateMessage *candidateMessage = (ARDICECandidateMessage *)message;
                [_peerConnection addICECandidate:candidateMessage.candidate];
                break;
            }
            
            case kARDSignalingMessageTypeBye:
            // Other client disconnected.
            // TODO(tkchin): support waiting in room for next client. For now just
            // disconnect.
            [self disconnect];
            break;
        }
    }
    
    //Send Singaling Message
    - (void)sendSignalingMessage:(ARDSignalingMessage *) message {
        if (_isInitiator) {
            [self sendSignalingMessageToRoomServer:message completionHandler:nil];
        } else {
            [self sendSignalingMessageToCollider:message];
        }
    }
    
    //Create Local Video Track
    - (RTCVideoTrack *)createLocalVideoTrack {
        // The iOS simulator doesn't provide any sort of camera capture
        // support or emulation (http://goo.gl/rHAnC1) so don't bother
        // trying to open a local stream.
        // TODO(tkchin): local video capture for OSX. See
        // https://code.google.com/p/webrtc/issues/detail?id=3417.
        
        RTCVideoTrack *localVideoTrack = nil;
        #if !TARGET_IPHONE_SIMULATOR && TARGET_OS_IPHONE
            NSString *cameraID = nil;
        
            for (AVCaptureDevice *captureDevice in
                 [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
                if (captureDevice.position == AVCaptureDevicePositionFront) {
                    cameraID = [captureDevice localizedName];
                    break;
                }
            }
        
            NSAssert(cameraID, @"Unable to get the front camera id");
        
            RTCVideoCapturer *capturer = [RTCVideoCapturer capturerWithDeviceName:cameraID];
            RTCMediaConstraints *mediaConstraints = [self defaultMediaStreamConstraints];
            RTCVideoSource *videoSource = [_factory videoSourceWithCapturer:capturer constraints:mediaConstraints];
            localVideoTrack = [_factory videoTrackWithID:@"ARDAMSv0" source:videoSource];
        #endif
        
        return localVideoTrack;
    }
    
    //Create Local Media Stream
    - (RTCMediaStream *)createLocalMediaStream {
        RTCMediaStream* localStream = [_factory mediaStreamWithLabel:@"ARDAMS"];
        
        RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];
        if (localVideoTrack) {
            [localStream addVideoTrack:localVideoTrack];
            [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
        }
        
        [localStream addAudioTrack:[_factory audioTrackWithID:@"ARDAMSa0"]];
        
        if (_isSpeakerEnabled) {
            [self enableSpeaker];
        }
        
        return localStream;
    }
    
    #pragma mark - Room server methods
    //Register with Room Server For Room ID
    - (void)registerWithRoomServerForRoomId:(NSString *)roomId completionHandler:(void (^)(ARDRegisterResponse *)) completionHandler {
        
        NSString *urlString = [NSString stringWithFormat:kARDRoomServerRegisterFormat, self.serverHostUrl, roomId];
        NSURL *roomURL = [NSURL URLWithString:urlString];
        NSLog(@"Registering with room server.");
        
        __weak ARDAppClient *weakSelf = self;
        [NSURLConnection sendAsyncPostToURL:roomURL withData:nil completionHandler:^(BOOL succeeded, NSData *data) {
            ARDAppClient *strongSelf = weakSelf;
            if (!succeeded) {
                //Error
                NSError *error = [self roomServerNetworkError];
                [strongSelf.delegate appClient:strongSelf didError:error];
                completionHandler(nil);
                return;
            }
            
            ARDRegisterResponse *response = [ARDRegisterResponse responseFromJSONData:data];
            completionHandler(response);
        }];
    }
    
    //Send Signaling Message to Room Server
    - (void)sendSignalingMessageToRoomServer:(ARDSignalingMessage *)message completionHandler:(void (^)(ARDMessageResponse *))completionHandler {
        NSData *data = [message JSONData];
        NSString *urlString = [NSString stringWithFormat: kARDRoomServerMessageFormat, self.serverHostUrl, _roomId, _clientId];
        NSURL *url = [NSURL URLWithString:urlString];
        NSLog(@"C->RS POST: %@", message);
        __weak ARDAppClient *weakSelf = self;
        
        [NSURLConnection sendAsyncPostToURL:url withData:data completionHandler:^(BOOL succeeded, NSData *data) {
            ARDAppClient *strongSelf = weakSelf;
            if (!succeeded) {
                //Error
              NSError *error = [self roomServerNetworkError];
              [strongSelf.delegate appClient:strongSelf didError:error];
              return;
            }
            ARDMessageResponse *response = [ARDMessageResponse responseFromJSONData:data];
            NSError *error = nil;
            switch (response.result) {
                case kARDMessageResultTypeSuccess:
                    break;

                case kARDMessageResultTypeUnknown:
                    error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain code:kARDAppClientErrorUnknown userInfo:@{ NSLocalizedDescriptionKey: @"Unknown error.", }];
              
                case kARDMessageResultTypeInvalidClient:
                    error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain code:kARDAppClientErrorInvalidClient userInfo:@{ NSLocalizedDescriptionKey: @"Invalid client.", }];
                    break;
                
                case kARDMessageResultTypeInvalidRoom:
                    error = [[NSError alloc] initWithDomain:kARDAppClientErrorDomain code:kARDAppClientErrorInvalidRoom userInfo:@{ NSLocalizedDescriptionKey: @"Invalid room.", }];
                    break;
            };
            
            if (error) {
                //Error
              [strongSelf.delegate appClient:strongSelf didError:error];
            }
            
            if (completionHandler) {
              completionHandler(response);
            }
        }];
    }
    
    //Unregister With Room Server
    - (void)unregisterWithRoomServer {
        NSString *urlString = [NSString stringWithFormat:kARDRoomServerByeFormat, self.serverHostUrl, _roomId, _clientId];
        NSURL *url = [NSURL URLWithString:urlString];
        NSLog(@"C->RS: BYE");
        //Make sure to do a POST
        [NSURLConnection sendAsyncPostToURL:url withData:nil completionHandler:^(BOOL succeeded, NSData *data) {
            if (succeeded) {
                NSLog(@"Unregistered from room server.");
            } else {
                NSLog(@"Failed to unregister from room server.");
            }
        }];
    }
    
    //Room Server Network Error
    - (NSError *)roomServerNetworkError {
        NSError *error =
        [[NSError alloc] initWithDomain:kARDAppClientErrorDomain code:kARDAppClientErrorNetwork userInfo:@{ NSLocalizedDescriptionKey: @"Room server network error", }];
        return error;
    }
    
    #pragma mark - Collider methods
    //Register with Collider / Websocket
    - (void)registerWithColliderIfReady {
        if (!self.isRegisteredWithRoomServer) {
            return;
        }
        // Open WebSocket connection.
        _channel = [[ARDWebSocketChannel alloc] initWithURL:_websocketURL restURL:_websocketRestURL delegate:self];
        [_channel registerForRoomId:_roomId clientId:_clientId];
    }
    
    //Send Signal Message to Collider
    - (void)sendSignalingMessageToCollider:(ARDSignalingMessage *) message {
        NSData *data = [message JSONData];
        [_channel sendData:data];
    }
    
    #pragma mark - Defaults
    //Default MediaStream Constraints
    - (RTCMediaConstraints *)defaultMediaStreamConstraints {
        RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil optionalConstraints:nil];
        return constraints;
    }
    
    //Default Answer
    - (RTCMediaConstraints *)defaultAnswerConstraints {
        return [self defaultOfferConstraints];
    }
    
    //Default Offer Constraints
    - (RTCMediaConstraints *)defaultOfferConstraints {
        NSArray *mandatoryConstraints = @[[[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"], [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:@"true"]];
        RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints optionalConstraints:nil];
        return constraints;
    }
    
    //Default Peer Connections
    - (RTCMediaConstraints *)defaultPeerConnectionConstraints {
        NSArray *optionalConstraints = @[[[RTCPair alloc] initWithKey:@"DtlsSrtpKeyAgreement" value:@"true"]];
        RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil optionalConstraints:optionalConstraints];
        return constraints;
    }
    
    //Default Stun Server
    - (RTCICEServer *)defaultSTUNServer {
        NSURL *defaultSTUNServerURL = [NSURL URLWithString:kARDDefaultSTUNServerUrl];
        return [[RTCICEServer alloc] initWithURI:defaultSTUNServerURL username:@"" password:@""];
    }
    
    #pragma mark - Audio mute/unmute
    //Mute Audio
    - (void)muteAudioIn {
        NSLog(@"audio muted");
        RTCMediaStream *localStream = _peerConnection.localStreams[0];
        self.defaultAudioTrack = localStream.audioTracks[0];
        [localStream removeAudioTrack:localStream.audioTracks[0]];
        [_peerConnection removeStream:localStream];
        [_peerConnection addStream:localStream];
    }
    
    //Umute Audio
    - (void)unmuteAudioIn {
        NSLog(@"audio unmuted");
        RTCMediaStream* localStream = _peerConnection.localStreams[0];
        [localStream addAudioTrack:self.defaultAudioTrack];
        [_peerConnection removeStream:localStream];
        [_peerConnection addStream:localStream];
        if (_isSpeakerEnabled) [self enableSpeaker];
    }
    
    #pragma mark - Video mute/unmute
    //Mude Video
    - (void)muteVideoIn {
        NSLog(@"video muted");
        RTCMediaStream *localStream = _peerConnection.localStreams[0];
        self.defaultVideoTrack = localStream.videoTracks[0];
        [localStream removeVideoTrack:localStream.videoTracks[0]];
        [_peerConnection removeStream:localStream];
        [_peerConnection addStream:localStream];
    }
    
    //Unmute Video
    - (void)unmuteVideoIn {
        NSLog(@"video unmuted");
        RTCMediaStream* localStream = _peerConnection.localStreams[0];
        [localStream addVideoTrack:self.defaultVideoTrack];
        [_peerConnection removeStream:localStream];
        [_peerConnection addStream:localStream];
    }
    
    #pragma mark - swap camera
    //Create Local Video Track Back Camera
    - (RTCVideoTrack *)createLocalVideoTrackBackCamera {
        RTCVideoTrack *localVideoTrack = nil;
        #if !TARGET_IPHONE_SIMULATOR && TARGET_OS_IPHONE
            //AVCaptureDevicePositionFront
            NSString *cameraID = nil;
            for (AVCaptureDevice *captureDevice in
                 [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
                if (captureDevice.position == AVCaptureDevicePositionBack) {
                    cameraID = [captureDevice localizedName];
                    break;
                }
            }
            NSAssert(cameraID, @"Unable to get the back camera id");
        
            RTCVideoCapturer *capturer = [RTCVideoCapturer capturerWithDeviceName:cameraID];
            RTCMediaConstraints *mediaConstraints = [self defaultMediaStreamConstraints];
            RTCVideoSource *videoSource = [_factory videoSourceWithCapturer:capturer constraints:mediaConstraints];
            localVideoTrack = [_factory videoTrackWithID:@"ARDAMSv0" source:videoSource];
        #endif
        return localVideoTrack;
    }
    
        //Swap Camera Front
    - (void)swapCameraToFront{
        RTCMediaStream *localStream = _peerConnection.localStreams[0];
        [localStream removeVideoTrack:localStream.videoTracks[0]];
        
        RTCVideoTrack *localVideoTrack = [self createLocalVideoTrack];
        
        if (localVideoTrack) {
            [localStream addVideoTrack:localVideoTrack];
            [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
        }
        [_peerConnection removeStream:localStream];
        [_peerConnection addStream:localStream];
    }
    
    //Swap Camera Back
    - (void)swapCameraToBack{
        RTCMediaStream *localStream = _peerConnection.localStreams[0];
        [localStream removeVideoTrack:localStream.videoTracks[0]];
        
        RTCVideoTrack *localVideoTrack = [self createLocalVideoTrackBackCamera];
        
        if (localVideoTrack) {
            [localStream addVideoTrack:localVideoTrack];
            [_delegate appClient:self didReceiveLocalVideoTrack:localVideoTrack];
        }
        [_peerConnection removeStream:localStream];
        [_peerConnection addStream:localStream];
    }
    
    #pragma mark - enable/disable speaker
    //Enable Speaker
    - (void)enableSpeaker {
        [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
        _isSpeakerEnabled = YES;
    }
    
    //Disable Speaker
    - (void)disableSpeaker {
        [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
        _isSpeakerEnabled = NO;
    }
    
@end
