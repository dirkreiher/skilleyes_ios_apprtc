//
//  SettingVC.swift
//  Skilleyes
//
//  Created by Mac HD on 28/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {

    //Outlets
    @IBOutlet var lblHead: UILabel!
    @IBOutlet var lblDevice: UILabel!
    @IBOutlet var lblCount: UILabel!
    @IBOutlet var txtFldSkil: UITextField!
    @IBOutlet var vewCircle: UIView!
    @IBOutlet var img1: UIImageView!
    @IBOutlet var img2: UIImageView!
    @IBOutlet var btnLive: UIButton!
    
    //Variable
    let STATUSBAR_COLOR = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    var counterValue = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtFldSkil.layer.cornerRadius = 15
        txtFldSkil.clipsToBounds = true
        
        vewCircle.layer.borderWidth = 1
        vewCircle.layer.borderColor = UIColor.darkGray.cgColor
        
        vewCircle.layer.cornerRadius = 15
        vewCircle.clipsToBounds = true
        
        self.imgCorner(img: img1)
        self.imgCorner(img: img2)
        
        changeLanguage()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //MARK: app language method
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            lblHead.text = "die Einstellungen"
            lblDevice.text = "Ihr Gerät"
            
            btnLive.setTitle("Live adjusting", for: .normal)
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            lblHead.text = "Settings"
            lblDevice.text = "Your device"
            
            btnLive.setTitle("Live adjusting", for: .normal)
        }
    }
    
    func imgCorner(img: UIImageView)
    {
        img.layer.borderWidth = 1
        img.layer.borderColor = UIColor.darkGray.cgColor
        
        img.layer.cornerRadius = 40
        img.clipsToBounds = true
    }
    
    //MARK: action method
    @IBAction func actionSave(_ sender: Any)
    {
        
    }
    
    @IBAction func actionBack(_ sender: Any)
    {
        self.view.endEditing(true)
        self.frostedViewController.view.endEditing(true)
        self.frostedViewController.presentMenuViewController()
    }
    
    @IBAction func actionPlus(_ sender: Any)
    {
        lblCount.text = "\(counterValue)"
        counterValue += 1;
        lblCount.text = "\(counterValue)"
    }
    
    @IBAction func actionMinus(_ sender: Any)
    {
        if(counterValue != 1){
            counterValue -= 1;
        }
        lblCount.text = "\(counterValue)"
    }

}
