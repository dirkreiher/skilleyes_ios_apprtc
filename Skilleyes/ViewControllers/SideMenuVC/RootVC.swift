//
//  RootVC.swift
//  Dr Dicted
//
//  Created by Apple on 17/02/18.
//  Copyright © 2018 Pixlr it solutions. All rights reserved.
//

import UIKit
import REFrostedViewController

class RootVC: REFrostedViewController {

    override func awakeFromNib()
    {
        self.contentViewController = self.storyboard?.instantiateViewController(withIdentifier: "contentController")
        self.menuViewController = self.storyboard?.instantiateViewController(withIdentifier: "menuController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
