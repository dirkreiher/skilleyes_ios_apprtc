//
//  IncomingCallVC.swift
//  Skilleyes
//
//  Created by Dirk Reiher on 08.01.19.
//  Copyright © 2019 Mac HD. All rights reserved.
//

import UIKit

class IncomingCallVC: UIViewController, CameraChooseDelegate {
    
    //Outlets
    @IBOutlet weak var callName: UILabel!
    
    //Variables
    let STATUSBAR_COLOR = #colorLiteral(red: 0, green: 0.5843137255, blue: 0.9568627451, alpha: 1)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
               
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
    }
    
    //Status Bar Color
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //If Camera is choosen
    func didTapCameraChoose(view: cameraChoosePopup, string: String) {
        //ViewController
        /*let vc = self.storyboard?.instantiateViewController(withIdentifier: "VideoChatVC") as? VideoChatVC

        //Check Mode
        if(string == "skilleyes") {
            vc?.mode = "skilleyes"
        } else if(string == "handy") {
            vc?.mode = "handy"
        }*/
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ARTCVideoChatViewController") as? ARTCVideoChatViewController
        vc?.roomName = "\(UserDefaults.standard.string(forKey: "FCMTOKEN")! as NSString)-stdby" as String
        
        //Check Mode
        /*if(string == "skilleyes") {
         vc?.mode = "skilleyes"
         } else if(string == "handy") {
         vc?.mode = "handy"
         }*/
        
        //Call View
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    //Call Event
    @IBAction func callBtn(_ sender: Any) {
        
        //Open Popup for Camera Choose
        let cameraChooseView = cameraChoosePopup.init(frame: self.view.frame)
        cameraChooseView.delegate = self
        self.view.addSubview(cameraChooseView)
    }
    
    //Cancel Event
    @IBAction func cancelBtn(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}
