//
//  SplashVC.swift
//  Skilleyes
//
//  Created by Mac HD on 22/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        perform(#selector(moveToNext), with: nil, afterDelay: 1.0)
    }
    
    //MARK: Next screen method
    @objc func moveToNext()
    {
        if isKeyPresentInUserDefaults(key: "UserInfo")
        {
            let objLoginVC:RootVC = storyboard?.instantiateViewController(withIdentifier: "RootVC") as! RootVC
            self.navigationController?.pushViewController(objLoginVC, animated: false)
            
            return
        }
        
        let objLoginVC:LoginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(objLoginVC, animated: false)
    }
    
    //Check user default value
    func isKeyPresentInUserDefaults(key: String) -> Bool
    {
        return UserDefaults.standard.object(forKey: key) != nil
    }

}
