//
//  SearchListVC.swift
//  Skilleyes
//
//  Created by Mac HD on 23/11/18.
//  Copyright © 2018 Mac HD. All rights reserved.
//

import UIKit

class SearchListVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {

    //Outlets
    @IBOutlet weak var vewMain: UIView!
    @IBOutlet var searchData: UISearchBar!
    @IBOutlet var tblVewList: UITableView!
    @IBOutlet var lblSkils: UILabel!
    @IBOutlet var lblPrice: UILabel!
    
    //VAriables
    var arrList = NSArray()
    var filteredList = NSArray()
    let STATUSBAR_COLOR = #colorLiteral(red: 0.8745098039, green: 0.8784313725, blue: 0.8823529412, alpha: 1)
    var listDictionary = [String: [String]]()
    var listSectionTitles = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        arrList = [["id":"1","name":"Graphic Design","price":"2 $"],["id":"2","name":"Photoshop","price":"2 $"],["id":"3","name":"Design","price":"2 $"],["id":"4","name":"Graphic Design","price":"2 $"],["id":"5","name":"Graphic Design","price":"2 $"],["id":"6","name":"Ios","price":"2 $"],["id":"7","name":"Bio","price":"2 $"],["id":"8","name":"yash","price":"2 $"],["id":"9","name":"Zeal","price":"2 $"],["id":"10","name":"Jabalpur","price":"2 $"],["id":"11","name":"Zeal","price":"2 $"]]
        
        self.filteredList = (self.arrList).mutableCopy() as! NSMutableArray
        print(self.filteredList)
        
//        for userDict in filteredList {
//            let title = String.init(format: "%@", (userDict as AnyObject).object(forKey: "name") as! CVarArg)
//            print(title)
//
//            let listKey = String(title.prefix(1))
//            if var listValues = listDictionary[listKey] {
//                listValues.append(title)
//                listDictionary[listKey] = listValues
//            } else {
//                listDictionary[listKey] = [title]
//            }
//        }
//
//        listSectionTitles = [String](listDictionary.keys)
//        print(listSectionTitles)
//        listSectionTitles = listSectionTitles.sorted(by: { $0 < $1 })
//        print(listSectionTitles)
        
        changeLanguage()
        
        //setup status bar
        self.setNeedsStatusBarAppearanceUpdate()
        setStatusBarBackgroundColor(color: STATUSBAR_COLOR)
        
        self.shadowView(vew: vewMain)
    }
    
    func firstCharOfFirstName(_ aDict: [String:String]) -> Character {
        return aDict["name"]!.first!
    }
    
    //MARK: Status bar text color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setStatusBarBackgroundColor(color:UIColor)  {
        let statusBar = (UIApplication.shared.value(forKey: "statusBarWindow") as AnyObject).value(forKey: "statusBar") as? UIView
        statusBar?.backgroundColor = color
    }
    
    //MARK: View shadow
    func shadowView(vew:UIView)
    {
        vew.layer.cornerRadius = 10
        vew.layer.masksToBounds = true
        vew.layer.shadowColor = UIColor.lightGray.cgColor
        vew.layer.shadowOffset = CGSize(width: -1, height: -1)
        vew.layer.shadowOpacity = 0.5
        vew.layer.shadowRadius = 1.0
    }
    
    //MARK: app language method
    func changeLanguage()
    {
        if (UserDefaults.standard.string(forKey: "Germany") != nil)
        {
            lblSkils.text = "Skills"
            lblPrice.text = "Price/min"
            
        }else if (UserDefaults.standard.string(forKey: "America") != nil)
        {
            lblSkils.text = "Skills"
            lblPrice.text = "Price/min"
        }
    }

    //MARK: searchbar delegate methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar == searchData {
            if searchText.count == 0 {
                
                searchBar.performSelector(onMainThread: #selector(self.resignFirstResponder), with: nil, waitUntilDone: false)
                self.filteredList = (self.arrList).mutableCopy() as! NSMutableArray
                print(self.filteredList)
                tblVewList.reloadData()
            } else {
                print("\(filteredList)")
                let predicate = NSPredicate(format: "name contains[c] %@", searchText)
                print(predicate)
                filteredList = arrList.filtered(using: predicate) as NSArray
                
                print(filteredList)
                tblVewList.reloadData()
                
            }
            tblVewList.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchData.resignFirstResponder()
    }
    
    //MARK: TableView delegate
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return listSectionTitles.count
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            return filteredList.count
//        let listKey = listSectionTitles[section]
//        if let listValues = listDictionary[listKey] {
//            return listValues.count
//        }
//        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblVewList.dequeueReusableCell(withIdentifier: "CellList") as! CellList
        
//        let listKey = listSectionTitles[indexPath.section]
//        print(listKey)
//        if let listValues = listDictionary[listKey] {
//            print(listValues)
//            cell.lblTitle.text = listValues[indexPath.row]
//        }
        
        let DictUser = self.filteredList[indexPath.row] as! NSDictionary
        print(DictUser)
        cell.lblTitle.text = String.init(format: "%@", DictUser.object(forKey: "name") as! CVarArg)
        cell.lblPrice.text = String.init(format: "%@", DictUser.object(forKey: "price") as! CVarArg)
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if(section == 0){
//        return 0.0
//        }
//        return 0.0
//    }
//
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return listSectionTitles[section]
//    }
//
//    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
//        return listSectionTitles
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let vc:SearchDescVC = storyboard?.instantiateViewController(withIdentifier: "SearchDescVC") as! SearchDescVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
//
//            let DictUser = self.filteredCat1[indexPath.row] as! NSDictionary
//            print(DictUser)
//            txtFldSubCategry.text = ""
//            txtFldCategry.text = String.init(format: "%@", DictUser.object(forKey: "name") as! CVarArg)
//
//            let id = String.init(format: "%@", DictUser.object(forKey: "id") as! CVarArg)
//            self.selectCatId = id
//            //  apiNotiTimer.invalidate()
//            self.getSubCategryList2(category_id: self.selectCatId)
//
//            searchCat1.text = ""
//            searchCat1.resignFirstResponder()
//            tbleVewCat1.isHidden = true
//            searchCat1.isHidden = true
//            vewCategry.isHidden = true
//            vewPopup.isHidden = false
//            vewMain.isHidden = false
//
//    }

}
