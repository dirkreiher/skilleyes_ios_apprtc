//
//  SMHUDView.swift
//  NFCScan
//
//  Created by WebvilleeMAC on 13/11/17.
//  Copyright © 2017 Webvillee. All rights reserved.
//

import UIKit

class SMHUDView: UIView {
var vie = UIView()
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func draw(_ rect: CGRect) {
        self.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        
        self.backgroundColor = UIColor.clear
        self.addSubview(vie)
        vie = UIView.init(frame: CGRect.init(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: 50))
        vie.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let lblRefresh = UILabel.init(frame: CGRect.init(x: self.frame.size.width/2-100, y: 15, width: 200, height: 20))
        lblRefresh.text = "Communicating With Server.."
        lblRefresh.textAlignment = .center
        lblRefresh.adjustsFontSizeToFitWidth = true
        lblRefresh.textColor = UIColor.white
        lblRefresh.font = UIFont.boldSystemFont(ofSize: 15)
        vie.addSubview(lblRefresh)
    }
    
    func showHud(){
        UIView.animate(withDuration: 0.50, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
            self.vie.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height-50, width: UIScreen.main.bounds.size.width, height: 50)
            
        }, completion: nil)
    }
    func hideHud(){

        UIView.animate(withDuration: 0.50, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
            //Set x position what ever you want
            self.vie.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: 50)
        }, completion: {
            (value: Bool) in
            self.removeFromSuperview()
        })
    }

}
